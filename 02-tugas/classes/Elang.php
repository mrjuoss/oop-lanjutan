<?php

require_once "traits/FlightTrait.php";
require_once "traits/HewanTrait.php";

class Elang
{
    use HewanTrait, FlightTrait;

    public function __construct($nama, $jumlahKaki, $keahlian, $attactPower, $defencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attactPower = $attactPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo "<br>";
        echo "=== INFO HEWAN === ";
        echo "<br>";
        echo "Jenis Hewan : " . get_class($this) . "<br>";
        echo "Nama Hewan : " . $this->nama . "<br>";
        echo "Jumlah Kaki : " . $this->jumlahKaki . "<br>";
        echo "Keahlian : " . $this->keahlian . "<br>";
        echo "Darah : " . $this->darah . "<br>";
        echo "Attack Power : " . $this->attactPower . "<br>";
        echo "Defense Power : " . $this->defencePower . "<br><br>";
        echo "<br>";
    }
}
