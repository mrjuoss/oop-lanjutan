<?php

require_once "classes/Elang.php";
require_once "classes/Harimau.php";

$elang = new Elang("Elang 1", 2, "Terbang Tinggi", 10, 5);
$harimau = new Harimau("Harimau 1", 4, "Lari Cepat", 7, 8);

echo $elang->getInfoHewan();
echo '</br>';
echo $harimau->getInfoHewan();
echo '</br>';

echo $elang->atraksi();
echo "<br />";
echo $harimau->atraksi();
echo "<br />";

echo $elang->serang($harimau);
echo $elang->diserang($harimau);
echo $harimau->serang($elang);
echo $harimau->diserang($elang);

echo $elang->getInfoHewan();
echo $harimau->getInfoHewan();
