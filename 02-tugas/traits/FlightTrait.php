<?php

trait FlightTrait
{
    public $attackPower;
    public $defencePower;

    // “darah sekarang – attackPower penyerang / defencePower yang diserang”
    public function serang($opponent)
    {
        // “harimau_1 sedang menyerang elang_3” atau “elang_3 sedang menyerang harimau_2”.
        echo $this->nama . " sedang menyerang " . $opponent->nama . "<br><br>";
    }

    public function diserang($opponent)
    {
        // “harimau_1 sedang diserang” atau “elang_3 sedang diserang”
        echo $this->nama . " sedang diserang" . "<br><br>";

        $this->darah = $this->darah - $opponent->attackPower / $this->defencePower;
    }
}
