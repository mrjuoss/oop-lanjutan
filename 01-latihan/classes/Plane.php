<?php

require_once "traits/EngineTrait.php";
require_once "traits/FlyableTrait.php";

class Plane
{
    use FlyableTrait, EngineTrait;
}
