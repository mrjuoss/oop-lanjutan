<?php

require_once "classes/Car.php";
require_once "classes/Helicopter.php";
require_once "classes/Plane.php";

$plane1 = new Plane();
echo "Plane ". $plane1->fly();
echo "<br />";
echo "Plane ". $plane1->info();


echo "<br />";
echo "<br />";

$helicopter1 = new Helicopter();
echo "Helicopter ". $helicopter1->fly();
echo "<br />";
echo "Helicopter ". $helicopter1->info();


echo "<br />";
echo "<br />";
$car1 = new Car();
echo "Car ". $car1->info();
