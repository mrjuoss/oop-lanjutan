<?php

class Product
{
    public $judul;
    public $penulis;
    public $penerbit;
    public $harga;

    public function __construct($judul, $penulis, $penerbit, $harga)
    {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
    }
}

class InfoProduct
{
    public function cetak(Product $product)
    {
        $str = "{$product->judul} | {$product->penulis} {$product->penerbit} (Rp. {$product->harga})";
        return $str;
    }
}


$product1 = new Product("Naruto", "Mashasi Kishimoto", "Japan Pub", 30000);

$infoProduct1 = new InfoProduct();
echo $infoProduct1->cetak($product1);
